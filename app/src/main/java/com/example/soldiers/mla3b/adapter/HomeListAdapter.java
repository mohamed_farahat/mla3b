package com.example.soldiers.mla3b.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.soldiers.mla3b.R;
import com.example.soldiers.mla3b.model.Ml3b;

import java.util.List;

/**
 * Created by Soldiers on 8/31/2017.
 */

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.GroundHolder> {

    List<Ml3b> groundList;

    public HomeListAdapter(List<Ml3b> groundList) {
        this.groundList = groundList;
    }

    @Override
    public GroundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.mla3b_row, parent, false);
        GroundHolder holder = new GroundHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(GroundHolder holder, int position) {
        Ml3b ground = groundList.get(position);
        holder.groundName.setText(ground.getGroundName());
        holder.groundAddress.setText(ground.getGroundAddress());
        holder.groundPrice.setText(ground.getGroundPrice());
        holder.groundPoster.setBackgroundResource(ground.getGroundPoster());
    }

    @Override
    public int getItemCount() {
        return groundList.size();
    }

    class GroundHolder extends RecyclerView.ViewHolder {


        TextView groundName, groundAddress, groundPrice;
        LinearLayout groundPoster;


        public GroundHolder(View itemView) {
            super(itemView);
            groundName = (TextView) itemView.findViewById(R.id.ground_name);
            groundAddress = (TextView) itemView.findViewById(R.id.ground_address);
            groundPrice = (TextView) itemView.findViewById(R.id.ground_price);
            groundPoster = (LinearLayout) itemView.findViewById(R.id.background);

        }
    }
}
