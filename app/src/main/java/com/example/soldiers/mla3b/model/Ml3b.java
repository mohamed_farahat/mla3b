package com.example.soldiers.mla3b.model;

/**
 * Created by Soldiers on 8/31/2017.
 */

public class Ml3b {

    public String groundName, groundAddress, groundPrice;
    public int groundPoster;

    public Ml3b() {
    }

    public Ml3b(String groundName, String groundAddress, String groundPrice, int groundPoster) {
        this.groundName = groundName;
        this.groundAddress = groundAddress;
        this.groundPrice = groundPrice;
        this.groundPoster = groundPoster;
    }

    public String getGroundName() {
        return groundName;
    }

    public String getGroundAddress() {
        return groundAddress;
    }

    public String getGroundPrice() {
        return groundPrice;
    }

    public int getGroundPoster() {
        return groundPoster;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }

    public void setGroundAddress(String groundAddress) {
        this.groundAddress = groundAddress;
    }

    public void setGroundPrice(String groundPrice) {
        this.groundPrice = groundPrice;
    }

    public void setGroundPoster(int groundPoster) {
        this.groundPoster = groundPoster;
    }
}
